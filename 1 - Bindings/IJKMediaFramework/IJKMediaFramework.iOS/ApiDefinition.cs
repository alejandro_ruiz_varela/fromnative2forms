﻿
using UIKit;
using Foundation;
using CoreGraphics;
using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace IJKMediaFramework.iOS
{
	// The first step to creating a binding is to add your native library ("libNativeLibrary.a")
	// to the project by right-clicking (or Control-clicking) the folder containing this source
	// file and clicking "Add files..." and then simply select the native library (or libraries)
	// that you want to bind.
	//
	// When you do that, you'll notice that MonoDevelop generates a code-behind file for each
	// native library which will contain a [LinkWith] attribute. MonoDevelop auto-detects the
	// architectures that the native library supports and fills in that information for you,
	// however, it cannot auto-detect any Frameworks or other system libraries that the
	// native library may depend on, so you'll need to fill in that information yourself.
	//
	// Once you've done that, you're ready to move on to binding the API...
	//
	//
	// Here is where you'd define your API definition for the native Objective-C library.
	//
	// For example, to bind the following Objective-C class:
	//
	//     @interface Widget : NSObject {
	//     }
	//
	// The C# binding would look like this:
	//
	//     [BaseType (typeof (NSObject))]
	//     interface Widget {
	//     }
	//
	// To bind Objective-C properties, such as:
	//
	//     @property (nonatomic, readwrite, assign) CGPoint center;
	//
	// You would add a property definition in the C# interface like so:
	//
	//     [Export ("center")]
	//     CGPoint Center { get; set; }
	//
	// To bind an Objective-C method, such as:
	//
	//     -(void) doSomething:(NSObject *)object atIndex:(NSInteger)index;
	//
	// You would add a method definition to the C# interface like so:
	//
	//     [Export ("doSomething:atIndex:")]
	//     void DoSomething (NSObject object, int index);
	//
	// Objective-C "constructors" such as:
	//
	//     -(id)initWithElmo:(ElmoMuppet *)elmo;
	//
	// Can be bound as:
	//
	//     [Export ("initWithElmo:")]
	//     IntPtr Constructor (ElmoMuppet elmo);
	//
	// For more information, see http://developer.xamarin.com/guides/ios/advanced_topics/binding_objective-c/
	//

	// @protocol IJKMediaPlayback <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface IJKMediaPlayback
	{
		// @required -(void)prepareToPlay;
		[Abstract]
		[Export("prepareToPlay")]
		void PrepareToPlay();

		// @required -(void)play;
		[Abstract]
		[Export("play")]
		void Play();

		// @required -(void)pause;
		[Abstract]
		[Export("pause")]
		void Pause();

		// @required -(void)stop;
		[Abstract]
		[Export("stop")]
		void Stop();

		// @required -(BOOL)isPlaying;
		[Abstract]
		[Export("isPlaying")]
		//[Verify(MethodToProperty)]
		bool IsPlaying { get; }

		// @required -(void)shutdown;
		[Abstract]
		[Export("shutdown")]
		void Shutdown();

		// @required -(void)setPauseInBackground:(BOOL)pause;
		[Abstract]
		[Export("setPauseInBackground:")]
		void SetPauseInBackground(bool pause);

		// @required @property (readonly, nonatomic) UIView * view;
		[Abstract]
		[Export("view")]
		UIView View { get; }

		// @required @property (nonatomic) NSTimeInterval currentPlaybackTime;
		[Abstract]
		[Export("currentPlaybackTime")]
		double CurrentPlaybackTime { get; set; }

		// @required @property (readonly, nonatomic) NSTimeInterval duration;
		[Abstract]
		[Export("duration")]
		double Duration { get; }

		// @required @property (readonly, nonatomic) NSTimeInterval playableDuration;
		[Abstract]
		[Export("playableDuration")]
		double PlayableDuration { get; }

		// @required @property (readonly, nonatomic) NSInteger bufferingProgress;
		[Abstract]
		[Export("bufferingProgress")]
		nint BufferingProgress { get; }

		// @required @property (readonly, nonatomic) BOOL isPreparedToPlay;
		[Abstract]
		[Export("isPreparedToPlay")]
		bool IsPreparedToPlay { get; }

		// @required @property (readonly, nonatomic) IJKMPMoviePlaybackState playbackState;
		[Abstract]
		[Export("playbackState")]
		IJKMPMoviePlaybackState PlaybackState { get; }

		// @required @property (readonly, nonatomic) IJKMPMovieLoadState loadState;
		[Abstract]
		[Export("loadState")]
		IJKMPMovieLoadState LoadState { get; }

		// @required @property (readonly, nonatomic) int64_t numberOfBytesTransferred;
		[Abstract]
		[Export("numberOfBytesTransferred")]
		long NumberOfBytesTransferred { get; }

		// @required @property (readonly, nonatomic) CGSize naturalSize;
		[Abstract]
		[Export("naturalSize")]
		CGSize NaturalSize { get; }

		// @required @property (nonatomic) IJKMPMovieScalingMode scalingMode;
		[Abstract]
		[Export("scalingMode", ArgumentSemantic.Assign)]
		IJKMPMovieScalingMode ScalingMode { get; set; }

		// @required @property (nonatomic) BOOL shouldAutoplay;
		[Abstract]
		[Export("shouldAutoplay")]
		bool ShouldAutoplay { get; set; }

		// @required @property (nonatomic) BOOL allowsMediaAirPlay;
		[Abstract]
		[Export("allowsMediaAirPlay")]
		bool AllowsMediaAirPlay { get; set; }

		// @required @property (nonatomic) BOOL isDanmakuMediaAirPlay;
		[Abstract]
		[Export("isDanmakuMediaAirPlay")]
		bool IsDanmakuMediaAirPlay { get; set; }

		// @required @property (readonly, nonatomic) BOOL airPlayMediaActive;
		[Abstract]
		[Export("airPlayMediaActive")]
		bool AirPlayMediaActive { get; }

		// @required @property (nonatomic) float playbackRate;
		[Abstract]
		[Export("playbackRate")]
		float PlaybackRate { get; set; }

		// @required -(UIImage *)thumbnailImageAtCurrentTime;
		[Abstract]
		[Export("thumbnailImageAtCurrentTime")]
		//[Verify(MethodToProperty)]
		UIImage ThumbnailImageAtCurrentTime { get; }
	}

	[Static]
	//[Verify(ConstantsInterfaceAssociation)]
	partial interface Constants
	{
		// extern NSString *const IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification __attribute__((visibility("default")));
		[Field("IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification", "__Internal")]
		NSString IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification { get; }

		// extern NSString *const IJKMPMoviePlayerScalingModeDidChangeNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerScalingModeDidChangeNotification", "__Internal")]
		NSString IJKMPMoviePlayerScalingModeDidChangeNotification { get; }

		// extern NSString *const IJKMPMoviePlayerPlaybackDidFinishNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerPlaybackDidFinishNotification", "__Internal")]
		NSString IJKMPMoviePlayerPlaybackDidFinishNotification { get; }

		// extern NSString *const IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey", "__Internal")]
		NSString IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey { get; }

		// extern NSString *const IJKMPMoviePlayerPlaybackStateDidChangeNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerPlaybackStateDidChangeNotification", "__Internal")]
		NSString IJKMPMoviePlayerPlaybackStateDidChangeNotification { get; }

		// extern NSString *const IJKMPMoviePlayerLoadStateDidChangeNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerLoadStateDidChangeNotification", "__Internal")]
		NSString IJKMPMoviePlayerLoadStateDidChangeNotification { get; }

		// extern NSString *const IJKMPMoviePlayerIsAirPlayVideoActiveDidChangeNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerIsAirPlayVideoActiveDidChangeNotification", "__Internal")]
		NSString IJKMPMoviePlayerIsAirPlayVideoActiveDidChangeNotification { get; }

		// extern NSString *const IJKMPMovieNaturalSizeAvailableNotification __attribute__((visibility("default")));
		[Field("IJKMPMovieNaturalSizeAvailableNotification", "__Internal")]
		NSString IJKMPMovieNaturalSizeAvailableNotification { get; }

		// extern NSString *const IJKMPMoviePlayerVideoDecoderOpenNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerVideoDecoderOpenNotification", "__Internal")]
		NSString IJKMPMoviePlayerVideoDecoderOpenNotification { get; }

		// extern NSString *const IJKMPMoviePlayerFirstVideoFrameRenderedNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerFirstVideoFrameRenderedNotification", "__Internal")]
		NSString IJKMPMoviePlayerFirstVideoFrameRenderedNotification { get; }

		// extern NSString *const IJKMPMoviePlayerFirstAudioFrameRenderedNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerFirstAudioFrameRenderedNotification", "__Internal")]
		NSString IJKMPMoviePlayerFirstAudioFrameRenderedNotification { get; }

		// extern NSString *const IJKMPMoviePlayerDidSeekCompleteNotification __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerDidSeekCompleteNotification", "__Internal")]
		NSString IJKMPMoviePlayerDidSeekCompleteNotification { get; }

		// extern NSString *const IJKMPMoviePlayerDidSeekCompleteTargetKey __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerDidSeekCompleteTargetKey", "__Internal")]
		NSString IJKMPMoviePlayerDidSeekCompleteTargetKey { get; }

		// extern NSString *const IJKMPMoviePlayerDidSeekCompleteErrorKey __attribute__((visibility("default")));
		[Field("IJKMPMoviePlayerDidSeekCompleteErrorKey", "__Internal")]
		NSString IJKMPMoviePlayerDidSeekCompleteErrorKey { get; }
	}

	// @interface IJKMediaUrlOpenData : NSObject
	[BaseType(typeof(NSObject))]
	interface IJKMediaUrlOpenData
	{
		// -(id)initWithUrl:(NSString *)url event:(IJKMediaEvent)event segmentIndex:(int)segmentIndex retryCounter:(int)retryCounter;
		[Export("initWithUrl:event:segmentIndex:retryCounter:")]
		IntPtr Constructor(string url, IJKMediaEvent @event, int segmentIndex, int retryCounter);

		// @property (readonly, nonatomic) IJKMediaEvent event;
		[Export("event")]
		IJKMediaEvent Event { get; }

		// @property (readonly, nonatomic) int segmentIndex;
		[Export("segmentIndex")]
		int SegmentIndex { get; }

		// @property (readonly, nonatomic) int retryCounter;
		[Export("retryCounter")]
		int RetryCounter { get; }

		// @property (retain, nonatomic) NSString * url;
		[Export("url", ArgumentSemantic.Retain)]
		string Url { get; set; }

		// @property (assign, nonatomic) int fd;
		[Export("fd")]
		int Fd { get; set; }

		// @property (nonatomic, strong) NSString * msg;
		[Export("msg", ArgumentSemantic.Strong)]
		string Msg { get; set; }

		// @property (nonatomic) int error;
		[Export("error")]
		int Error { get; set; }

		// @property (getter = isHandled, nonatomic) BOOL handled;
		[Export("handled")]
		bool Handled { [Bind("isHandled")] get; set; }

		// @property (getter = isUrlChanged, nonatomic) BOOL urlChanged;
		[Export("urlChanged")]
		bool UrlChanged { [Bind("isUrlChanged")] get; set; }
	}

	// @protocol IJKMediaUrlOpenDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface IJKMediaUrlOpenDelegate
	{
		// @required -(void)willOpenUrl:(IJKMediaUrlOpenData *)urlOpenData;
		[Abstract]
		[Export("willOpenUrl:")]
		void WillOpenUrl(IJKMediaUrlOpenData urlOpenData);
	}

	// @protocol IJKMediaNativeInvokeDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface IJKMediaNativeInvokeDelegate
	{
		// @required -(int)invoke:(IJKMediaEvent)event attributes:(NSDictionary *)attributes;
		[Abstract]
		[Export("invoke:attributes:")]
		int Attributes(IJKMediaEvent @event, NSDictionary attributes);
	}

	// @interface IJKFFMonitor : NSObject
	[BaseType(typeof(NSObject))]
	interface IJKFFMonitor
	{
		// @property (nonatomic) NSDictionary * mediaMeta;
		[Export("mediaMeta", ArgumentSemantic.Assign)]
		NSDictionary MediaMeta { get; set; }

		// @property (nonatomic) NSDictionary * videoMeta;
		[Export("videoMeta", ArgumentSemantic.Assign)]
		NSDictionary VideoMeta { get; set; }

		// @property (nonatomic) NSDictionary * audioMeta;
		[Export("audioMeta", ArgumentSemantic.Assign)]
		NSDictionary AudioMeta { get; set; }

		// @property (readonly, nonatomic) int64_t duration;
		[Export("duration")]
		long Duration { get; }

		// @property (readonly, nonatomic) int64_t bitrate;
		[Export("bitrate")]
		long Bitrate { get; }

		// @property (readonly, nonatomic) float fps;
		[Export("fps")]
		float Fps { get; }

		// @property (readonly, nonatomic) int width;
		[Export("width")]
		int Width { get; }

		// @property (readonly, nonatomic) int height;
		[Export("height")]
		int Height { get; }

		// @property (readonly, nonatomic) NSString * vcodec;
		[Export("vcodec")]
		string Vcodec { get; }

		// @property (readonly, nonatomic) NSString * acodec;
		[Export("acodec")]
		string Acodec { get; }

		// @property (readonly, nonatomic) int sampleRate;
		[Export("sampleRate")]
		int SampleRate { get; }

		// @property (readonly, nonatomic) int64_t channelLayout;
		[Export("channelLayout")]
		long ChannelLayout { get; }

		// @property (nonatomic) int tcpError;
		[Export("tcpError")]
		int TcpError { get; set; }

		// @property (nonatomic) NSString * remoteIp;
		[Export("remoteIp")]
		string RemoteIp { get; set; }

		// @property (nonatomic) int httpError;
		[Export("httpError")]
		int HttpError { get; set; }

		// @property (nonatomic) NSString * httpUrl;
		[Export("httpUrl")]
		string HttpUrl { get; set; }

		// @property (nonatomic) NSString * httpHost;
		[Export("httpHost")]
		string HttpHost { get; set; }

		// @property (nonatomic) int httpCode;
		[Export("httpCode")]
		int HttpCode { get; set; }

		// @property (nonatomic) int64_t httpOpenTick;
		[Export("httpOpenTick")]
		long HttpOpenTick { get; set; }

		// @property (nonatomic) int64_t httpSeekTick;
		[Export("httpSeekTick")]
		long HttpSeekTick { get; set; }

		// @property (nonatomic) int httpOpenCount;
		[Export("httpOpenCount")]
		int HttpOpenCount { get; set; }

		// @property (nonatomic) int httpSeekCount;
		[Export("httpSeekCount")]
		int HttpSeekCount { get; set; }

		// @property (nonatomic) int64_t lastHttpOpenDuration;
		[Export("lastHttpOpenDuration")]
		long LastHttpOpenDuration { get; set; }

		// @property (nonatomic) int64_t lastHttpSeekDuration;
		[Export("lastHttpSeekDuration")]
		long LastHttpSeekDuration { get; set; }

		// @property (nonatomic) int64_t prepareStartTick;
		[Export("prepareStartTick")]
		long PrepareStartTick { get; set; }

		// @property (nonatomic) int64_t prepareDuration;
		[Export("prepareDuration")]
		long PrepareDuration { get; set; }

		// @property (nonatomic) int64_t firstVideoFrameLatency;
		[Export("firstVideoFrameLatency")]
		long FirstVideoFrameLatency { get; set; }

		// @property (nonatomic) int64_t lastPrerollStartTick;
		[Export("lastPrerollStartTick")]
		long LastPrerollStartTick { get; set; }

		// @property (nonatomic) int64_t lastPrerollDuration;
		[Export("lastPrerollDuration")]
		long LastPrerollDuration { get; set; }
	}

	// @interface IJKFFOptions : NSObject
	[BaseType(typeof(NSObject))]
	interface IJKFFOptions
	{
		// +(IJKFFOptions *)optionsByDefault;
		[Static]
		[Export("optionsByDefault")]
		//[Verify(MethodToProperty)]
		IJKFFOptions OptionsByDefault { get; }

		// -(void)applyTo:(struct IjkMediaPlayer *)mediaPlayer;
		//[Export("applyTo:")]
		//unsafe void ApplyTo(IjkMediaPlayer mediaPlayer);

		// -(void)setOptionValue:(NSString *)value forKey:(NSString *)key ofCategory:(IJKFFOptionCategory)category;
		[Export("setOptionValue:forKey:ofCategory:")]
		void SetOptionValue(string value, string key, IJKFFOptionCategory category);

		// -(void)setOptionIntValue:(int64_t)value forKey:(NSString *)key ofCategory:(IJKFFOptionCategory)category;
		[Export("setOptionIntValue:forKey:ofCategory:")]
		void SetOptionIntValue(long value, string key, IJKFFOptionCategory category);

		// -(void)setFormatOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setFormatOptionValue:forKey:")]
		void SetFormatOptionValue(string value, string key);

		// -(void)setCodecOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setCodecOptionValue:forKey:")]
		void SetCodecOptionValue(string value, string key);

		// -(void)setSwsOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setSwsOptionValue:forKey:")]
		void SetSwsOptionValue(string value, string key);

		// -(void)setPlayerOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setPlayerOptionValue:forKey:")]
		void SetPlayerOptionValue(string value, string key);

		// -(void)setFormatOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setFormatOptionIntValue:forKey:")]
		void SetFormatOptionIntValue(long value, string key);

		// -(void)setCodecOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setCodecOptionIntValue:forKey:")]
		void SetCodecOptionIntValue(long value, string key);

		// -(void)setSwsOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setSwsOptionIntValue:forKey:")]
		void SetSwsOptionIntValue(long value, string key);

		// -(void)setPlayerOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setPlayerOptionIntValue:forKey:")]
		void SetPlayerOptionIntValue(long value, string key);

		// @property (nonatomic) BOOL showHudView;
		[Export("showHudView")]
		bool ShowHudView { get; set; }
	}

	// @interface IJKFFMoviePlayerController : NSObject <IJKMediaPlayback>
	[BaseType(typeof(NSObject))]
	interface IJKFFMoviePlayerController : IJKMediaPlayback
	{
		// -(id)initWithContentURL:(NSURL *)aUrl withOptions:(IJKFFOptions *)options;
		[Export("initWithContentURL:withOptions:")]
		IntPtr Constructor(NSUrl aUrl, IJKFFOptions options);

		// -(id)initWithContentURLString:(NSString *)aUrlString withOptions:(IJKFFOptions *)options;
		[Export("initWithContentURLString:withOptions:")]
		IntPtr Constructor(string aUrlString, IJKFFOptions options);

		// -(void)prepareToPlay;
		[Export("prepareToPlay")]
		void PrepareToPlay();

		// -(void)play;
		[Export("play")]
		void Play();

		// -(void)pause;
		[Export("pause")]
		void Pause();

		// -(void)stop;
		[Export("stop")]
		void Stop();

		// -(BOOL)isPlaying;
		[Export("isPlaying")]
		//[Verify(MethodToProperty)]
		bool IsPlaying { get; }

		// -(void)setPauseInBackground:(BOOL)pause;
		[Export("setPauseInBackground:")]
		void SetPauseInBackground(bool pause);

		// -(BOOL)isVideoToolboxOpen;
		[Export("isVideoToolboxOpen")]
		//[Verify(MethodToProperty)]
		bool IsVideoToolboxOpen { get; }

		// +(void)setLogReport:(BOOL)preferLogReport;
		[Static]
		[Export("setLogReport:")]
		void SetLogReport(bool preferLogReport);

		// +(void)setLogLevel:(IJKLogLevel)logLevel;
		[Static]
		[Export("setLogLevel:")]
		void SetLogLevel(IJKLogLevel logLevel);

		// +(BOOL)checkIfFFmpegVersionMatch:(BOOL)showAlert;
		[Static]
		[Export("checkIfFFmpegVersionMatch:")]
		bool CheckIfFFmpegVersionMatch(bool showAlert);

		// +(BOOL)checkIfPlayerVersionMatch:(BOOL)showAlert version:(NSString *)version;
		[Static]
		[Export("checkIfPlayerVersionMatch:version:")]
		bool CheckIfPlayerVersionMatch(bool showAlert, string version);

		// @property (readonly, nonatomic) CGFloat fpsInMeta;
		[Export("fpsInMeta")]
		nfloat FpsInMeta { get; }

		// @property (readonly, nonatomic) CGFloat fpsAtOutput;
		[Export("fpsAtOutput")]
		nfloat FpsAtOutput { get; }

		// @property (nonatomic) BOOL shouldShowHudView;
		[Export("shouldShowHudView")]
		bool ShouldShowHudView { get; set; }

		// -(void)setOptionValue:(NSString *)value forKey:(NSString *)key ofCategory:(IJKFFOptionCategory)category;
		[Export("setOptionValue:forKey:ofCategory:")]
		void SetOptionValue(string value, string key, IJKFFOptionCategory category);

		// -(void)setOptionIntValue:(int64_t)value forKey:(NSString *)key ofCategory:(IJKFFOptionCategory)category;
		[Export("setOptionIntValue:forKey:ofCategory:")]
		void SetOptionIntValue(long value, string key, IJKFFOptionCategory category);

		// -(void)setFormatOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setFormatOptionValue:forKey:")]
		void SetFormatOptionValue(string value, string key);

		// -(void)setCodecOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setCodecOptionValue:forKey:")]
		void SetCodecOptionValue(string value, string key);

		// -(void)setSwsOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setSwsOptionValue:forKey:")]
		void SetSwsOptionValue(string value, string key);

		// -(void)setPlayerOptionValue:(NSString *)value forKey:(NSString *)key;
		[Export("setPlayerOptionValue:forKey:")]
		void SetPlayerOptionValue(string value, string key);

		// -(void)setFormatOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setFormatOptionIntValue:forKey:")]
		void SetFormatOptionIntValue(long value, string key);

		// -(void)setCodecOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setCodecOptionIntValue:forKey:")]
		void SetCodecOptionIntValue(long value, string key);

		// -(void)setSwsOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setSwsOptionIntValue:forKey:")]
		void SetSwsOptionIntValue(long value, string key);

		// -(void)setPlayerOptionIntValue:(int64_t)value forKey:(NSString *)key;
		[Export("setPlayerOptionIntValue:forKey:")]
		void SetPlayerOptionIntValue(long value, string key);

		[Wrap("WeakSegmentOpenDelegate")]
		IJKMediaUrlOpenDelegate SegmentOpenDelegate { get; set; }

		// @property (retain, nonatomic) id<IJKMediaUrlOpenDelegate> segmentOpenDelegate;
		[NullAllowed, Export("segmentOpenDelegate", ArgumentSemantic.Retain)]
		NSObject WeakSegmentOpenDelegate { get; set; }

		[Wrap("WeakTcpOpenDelegate")]
		IJKMediaUrlOpenDelegate TcpOpenDelegate { get; set; }

		// @property (retain, nonatomic) id<IJKMediaUrlOpenDelegate> tcpOpenDelegate;
		[NullAllowed, Export("tcpOpenDelegate", ArgumentSemantic.Retain)]
		NSObject WeakTcpOpenDelegate { get; set; }

		[Wrap("WeakHttpOpenDelegate")]
		IJKMediaUrlOpenDelegate HttpOpenDelegate { get; set; }

		// @property (retain, nonatomic) id<IJKMediaUrlOpenDelegate> httpOpenDelegate;
		[NullAllowed, Export("httpOpenDelegate", ArgumentSemantic.Retain)]
		NSObject WeakHttpOpenDelegate { get; set; }

		[Wrap("WeakLiveOpenDelegate")]
		IJKMediaUrlOpenDelegate LiveOpenDelegate { get; set; }

		// @property (retain, nonatomic) id<IJKMediaUrlOpenDelegate> liveOpenDelegate;
		[NullAllowed, Export("liveOpenDelegate", ArgumentSemantic.Retain)]
		NSObject WeakLiveOpenDelegate { get; set; }

		[Wrap("WeakNativeInvokeDelegate")]
		IJKMediaNativeInvokeDelegate NativeInvokeDelegate { get; set; }

		// @property (retain, nonatomic) id<IJKMediaNativeInvokeDelegate> nativeInvokeDelegate;
		[NullAllowed, Export("nativeInvokeDelegate", ArgumentSemantic.Retain)]
		NSObject WeakNativeInvokeDelegate { get; set; }

		// -(void)didShutdown;
		[Export("didShutdown")]
		void DidShutdown();

		// @property (readonly, nonatomic) IJKFFMonitor * monitor;
		[Export("monitor")]
		IJKFFMonitor Monitor { get; }
	}
}
