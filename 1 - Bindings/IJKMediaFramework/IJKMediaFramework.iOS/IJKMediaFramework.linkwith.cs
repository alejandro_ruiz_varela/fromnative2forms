// WARNING: This feature is deprecated. Use the "Native References" folder instead.
// Right-click on the "Native References" folder, select "Add Native Reference",
// and then select the static library or framework that you'd like to bind.
//
// Once you've added your static library or framework, right-click the library or
// framework and select "Properties" to change the LinkWith values.

using ObjCRuntime;

[assembly: LinkWith ("IJKMediaFramework.a", SmartLink = true, Frameworks = "AudioToolbox AVFoundation CoreGraphics CoreMedia CoreVideo MediaPlayer MobileCoreServices OpenGLES QuartzCore UIKit VideoToolbox", LinkerFlags = "-lz", ForceLoad = true)]
