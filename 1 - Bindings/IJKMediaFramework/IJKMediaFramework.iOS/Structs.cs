﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace IJKMediaFramework.iOS
{
	[Native]
public enum IJKMPMovieScalingMode : ulong
{
	None,
	AspectFit,
	AspectFill,
	Fill
}

[Native]
public enum IJKMPMoviePlaybackState : ulong
{
	Stopped,
	Playing,
	Paused,
	Interrupted,
	SeekingForward,
	SeekingBackward
}

[Native]
public enum IJKMPMovieLoadState : ulong
{
	Unknown = 0,
	Playable = 1 << 0,
	PlaythroughOK = 1 << 1,
	Stalled = 1 << 2
}

[Native]
public enum IJKMPMovieFinishReason : ulong
{
	PlaybackEnded,
	PlaybackError,
	UserExited
}

[Native]
public enum IJKMPMovieTimeOption : ulong
{
	NearestKeyFrame,
	Exact
}

[Native]
public enum IJKMediaEvent : ulong
{
	Event_WillHttpOpen = 1,
	Event_DidHttpOpen = 2,
	Event_WillHttpSeek = 3,
	Event_DidHttpSeek = 4,
	Ctrl_WillTcpOpen = 131073,
	Ctrl_DidTcpOpen = 131074,
	Ctrl_WillHttpOpen = 131075,
	Ctrl_WillLiveOpen = 131077,
	Ctrl_WillConcatSegmentOpen = 131079
}

public enum IJKFFOptionCategory : ulong
{
	Format = 1,
	Codec = 2,
	Sws = 3,
	Player = 4,
	Swr = 5
}

public enum IJKAVDiscard
{
	None = -16,
	Default = 0,
	Nonref = 8,
	Bidir = 16,
	Nonkey = 32,
	All = 48
}

public enum IJKLogLevel : ulong
{
	Unknown = 0,
	Default = 1,
	Verbose = 2,
	Debug = 3,
	Info = 4,
	Warn = 5,
	Error = 6,
	Fatal = 7,
	Silent = 8
}

static class CFunctions
{
	// extern void IJKFFIOStatDebugCallback (const char *url, int type, int bytes);
	[DllImport ("__Internal")]
	//[Verify (PlatformInvoke)]
	static extern unsafe void IJKFFIOStatDebugCallback (sbyte* url, int type, int bytes);

	// extern void IJKFFIOStatRegister (void (* cb)(const char *, int, int));
	[DllImport ("__Internal")]
	//[Verify (PlatformInvoke)]
	static extern unsafe void IJKFFIOStatRegister (Action<IntPtr, int, int> cb);

	// extern void IJKFFIOStatCompleteDebugCallback (const char *url, int64_t read_bytes, int64_t total_size, int64_t elpased_time, int64_t total_duration);
	[DllImport ("__Internal")]
	//[Verify (PlatformInvoke)]
	static extern unsafe void IJKFFIOStatCompleteDebugCallback (sbyte* url, long read_bytes, long total_size, long elpased_time, long total_duration);

	// extern void IJKFFIOStatCompleteRegister (void (* cb)(const char *, int64_t, int64_t, int64_t, int64_t));
	[DllImport ("__Internal")]
	//[Verify (PlatformInvoke)]
	static extern unsafe void IJKFFIOStatCompleteRegister (Action<IntPtr, long, long, long, long> cb);
}
}
